export interface IProducts {
  availableForSale: boolean;
  id: string;
  description: string;
  images: IProductsImages[];
  title: string;
  variants: IProductsVariants[];
}

export interface IProductsImages {
  id: string;
  src: string;
}

export interface IProductsVariants {
  id: string;
  image: IProductsImages[];
  price: string;
  sku: string;
}

export interface IProductsVariantImages {
  id: string;
  src: string;
}
