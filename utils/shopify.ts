import Client from 'shopify-buy';

const client = Client.buildClient({
  domain: process.env.NEXT_PUBLIC_SHOPIFY_DOMAIN as string,
  storefrontAccessToken: process.env
    .NEXT_PUBLIC_SHOPIFY_STOREFRONT_TOKEN as string
});

export { client };
