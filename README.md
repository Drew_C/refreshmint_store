**Next JS Shopify  Q2 Rock**

The backend for this project [QA Mint Mobile](https://qa-mm.myshopify.com/)

Technologies used:

 -  [Next JS](https://nextjs.org/)
 -  [Shopify JS Buy SDK](https://shopify.github.io/js-buy-sdk/)
 -  Sass
 -  Typescript
 -  Cypress Testing  

 
TODOS:  
[x] - Setup QA Mint Mobile Store on Shopify  
[x] - Create-next-app  
[x] - Install Shopify SDK  
[x] - Install Cypress Testing